import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import "./MovieItem.css";


const IMG_API = "https://image.tmdb.org/t/p/w500";


export default function MoviteItem(props) {
    const {movie} = props;
    const {viewMovieInfo} = props;
    const {movieId} = props;

    return (
        <div className="col-3 mt-5 movie-item">
            <Card onClick={() => props.viewMovieInfo(props.movieId)}>
            <button type="button" className="btn btn-danger btn-circle btn-lg"><i className="glyphicon glyphicon-heart"></i>{movie.vote_average}</button>
                <Card.Img src={IMG_API + movie.poster_path} alt="" />
                <Card.Body>
                    <Card.Text className="text-left">
                        <p className="p-height">{movie.original_title}</p>
                        <span><strong>{movie.original_language}</strong></span>
                    </Card.Text>
                </Card.Body>
            </Card>
        </div>
        
    )
}
