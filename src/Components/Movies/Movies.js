import React, { useEffect, useState } from 'react';
import MovieItem from '../MovieItem/MoviteItem';
import "./Movies.css";
import { Card, Container, Row, Button } from 'react-bootstrap';



export default function Movies(props) {
    const { movies } = props;
    const { genreID } = props;
    // const {viewMovieInfo} = props;
    const [numberOfElements, setNumberOfElements] = useState(4);

    const loadMore = () => {
        setNumberOfElements(numberOfElements + 4);
    }

    const slice = movies.slice(0, numberOfElements);

    return (
        <div className="row movies">
            {slice.map((movie) => (
                <MovieItem key={movie.id} movie={movie} viewMovieInfo={props.viewMovieInfo} movieId={movie.id} />
            ))
            }
            <Row className=" d-flex justify-content-center">
                <Button className="mt-5 mb-5 col-md-4 load-more" onClick={() => loadMore()}>Load more</Button>
            </Row>
            {genreID}
        </div>
    )
}
