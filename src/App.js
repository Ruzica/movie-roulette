import './App.css';
import Movies from './Components/Movies/Movies';
import MovieInfo from './Components/MovieInfo/MovieInfo';
import Axios from 'axios';
import { useState } from 'react';
import React from "react";
import { Card, Container, Row, Button } from 'react-bootstrap';
import MovieGenerator from './Components/MovieGenerator/MovieGenerator';
import RandomMovie from './Components/RandomMovie/RandomMovie';


const URL_API = "https://api.themoviedb.org/3/discover/movie?api_key=faedada989a9743908fb8e710995ae0e&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_watch_monetization_types=flatrate";
const api_key = "faedada989a9743908fb8e710995ae0e";

function App() {

  const [movies, setMovies] = useState(null);
  const [currentMovie, setCurrentMovie] = useState(null);
  const [showGenerator, setShowGenerator] = useState(false);
  const [genre, setGenre] = useState();
  const [isRolled, setIsRolled] = useState(false);
  const [randomMovie, setRandomMovie] = useState(null);

  React.useEffect(() => {
    Axios.get(URL_API).then((response) => {
      setMovies(response.data.results)
      //  console.log(response.data.results[1].genre_ids);
    });
  }, []);

  if (!movies) return null;

  const viewMovieInfo = (id) => {
    const filteredMovie = movies.filter(movie => movie.id === id)
    const newCurrentMovie = filteredMovie.length > 0 ? filteredMovie[0] : null
    setCurrentMovie(newCurrentMovie);
  }

  const closeMovieInfo = () => {
    setCurrentMovie(null);
  }

  const displayGenerator = () => {
    setShowGenerator(true);
    console.log("test 2");
  }

  const showRandomMovie = (genreID) => {
    const intGenreID = parseInt(genreID);
    setIsRolled(true);
    setShowGenerator(false);
    const filteredMoviesIds = movies.filter(movie => movie.genre_ids.includes(intGenreID)).map(filteredMovie => filteredMovie.id);
    const randomMovieId = filteredMoviesIds[Math.floor(Math.random() * filteredMoviesIds.length)];
    setRandomMovie(randomMovieId);
    alert(JSON.stringify(randomMovieId));
  }

  return (
    <div className="App">
      {console.log('I was triggered during render')}
      <div className="container">
        {currentMovie === null ? <Movies movies={movies} viewMovieInfo={viewMovieInfo} /> : <MovieInfo movies={movies} isRolled={isRolled} randomMovieId={randomMovie} currentMovie={currentMovie} closeMovieInfo={closeMovieInfo}></MovieInfo>}
        <Button className="random-generator-btn" onClick={() => displayGenerator()}>🎲</Button>
        {showGenerator === true ? <MovieGenerator viewMovieInfo={viewMovieInfo} showRandomMovie={showRandomMovie} movies={movies} /> : null}
      </div>
    </div>
  );
}

export default App;
