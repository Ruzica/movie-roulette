import React from 'react';
import { Card, Container, Row, Button } from 'react-bootstrap';
import "./MovieGenerator.css";
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { useState } from 'react';
import Axios from 'axios';
import NativeSelect from '@mui/material/NativeSelect';

const API_GENRES = "https://api.themoviedb.org/3/genre/movie/list?api_key=faedada989a9743908fb8e710995ae0e&language=en-US";

export default function MovieGenerator(props) {
    const movies = { props };
    const [genres, setGenres] = useState(null);
    const [genre, setGenre] = useState(null);
    const [moviesGenres, setMoviesGenres] = useState(props.movies);
    
    let finalGenres;

    React.useEffect(() => {
        Axios.get(API_GENRES).then((response) => {
            setGenres(response.data.genres)
        });
    }, []);

    if (!genres) return null;

    const handleChange = (event) => {
        setGenre(event.target.value);
        console.log(event.target.value);

    };

    const getMovieGenres = () => {
        let newMoviesGenres = moviesGenres.map(movieGenre => movieGenre.genre_ids);
        let mergedNewMoviesGenres = [].concat.apply([], newMoviesGenres);
        finalGenres = [...new Set(mergedNewMoviesGenres)];
    }

    getMovieGenres();

    return (
        <div className="movie-item movie-generator">
            <Card>
                <Card.Body>
                    <Card.Text className="text-left border">
                        <span><strong>Movie Roulette</strong></span>
                    </Card.Text>
                    <FormControl component="fieldset">
                        <NativeSelect onChange={handleChange}>
                            <option>Select genre: </option>
                            {genres.map(genre => (
                              finalGenres.includes(genre.id) ? <option key={genre.id} value={genre.id}>{genre.name}</option> : null  
                            ))}
                        </NativeSelect>
                        <Button onClick={() => props.showRandomMovie(genre)}>Roll</Button>
                    </FormControl>
                </Card.Body>
            </Card>
        </div>
    )
};
