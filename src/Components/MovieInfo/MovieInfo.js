import React from 'react';
import { Card, Container, Row, Button } from 'react-bootstrap';
import "./MovieInfo.css";
import Axios from 'axios';
import { useState, useEffect } from 'react';
import Rating from '@mui/material/Rating';
import "../../useLocalStorage";
import { useLocalStorage } from '../../useLocalStorage';


const IMG_API = "https://image.tmdb.org/t/p/w500";

export default function MovieInfo(props) {
    const [rate, setRate] = useLocalStorage(props.currentMovie.id, 0);
    //let movie = props.isRolled === false ? props.currentMovie : props.movies[props.randomMovieId];

    // const [companies, setCompanies] = useState(null);

    //const COMPANIES_API = `https://api.themoviedb.org/3/movie/${props.currentMovie.id}?api_key=faedada989a9743908fb8e710995ae0e`;


    /*  React.useEffect(() => {
          Axios.get(COMPANIES_API).then((response) => {
      //        setCompanies(response.data.results.production_companies)
      //        console.log(response.data.production_companies);
          });
      }, []);
  
      if (!companies) return null;*/

     
      let movie = props.isRolled === true ? props.movies[props.randomMovieId] : props.currentMovie;



    return (
        <div>
            <div id="movie-card-list">
                <div className="movie-card" src={IMG_API + movie.poster_path}>
                    <button type="button" className="btn btn-danger btn-circle btn-lg" onClick={() => props.closeMovieInfo()}>X</button>
                    <Card.Img className="card-img-info" src={IMG_API + movie.poster_path} alt="" />
                    <div className="movie-card__overlay"></div>
                    <div className="movie-card__content">
                        <div className="movie-card__header">
                            <h1 className="movie-card__title mt-5">{movie.original_title} ({movie.release_date.substring(0, 4)})</h1>
                            <h4 className="movie-card__info">Language: {props.currentMovie.original_language}</h4>
                        </div>
                        <p className="movie-card__desc">{movie.overview}</p>
                        <h4 className="movie-card__info">Rating: <i> {movie.vote_average} </i></h4>
                        <h4 className="movie-card__info mt-3">Popularity: <i> {movie.popularity} </i></h4>
                        <h4 className="movie-card__info mt-3">Productions companies: <i> {movie.popularity} </i></h4>
                        <h4 className="movie-card__info mt-3">Productions companies: <i> {movie.id} </i></h4>
                        <div className="mt-5 rate-section" >
                            <Rating key={movie.id} name="customized-10" value={rate} max={10} onClick={event => setRate(event.target.value)} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
